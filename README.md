# Arduino基础环境

- 默认atmega328p芯片，其他环境请自行修改`pins_arduino.h`

## 环境安装
```shell
> apt update
> apt install gcc-avr avr-libc avrdude gdb-avr
```
## 编译代码
```
> make clean && make
```
## 烧录
```
> avrdude -v -patmega328p -carduino -PCOM3 -b115200 -D -Uflash:w:main.hex:i
```

